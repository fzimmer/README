## [DRAFT] Quarterly Priorities

This content is meant to communicate how I intend to allocate my time. I review it weekly, but it should largely remain consistent on the time-scales of quarters.
The time allocation is not final yet and subject to change.

| Theme                                      | Notes                                                                                                        | Percent |
|--------------------------------------------|--------------------------------------------------------------------------------------------------------------|---------|
| Sharding working group                     | Establish process, ensure product perspective, organize/kickoff project delivery                             | 30%     |
| Product Management Leadership Priorities   | Group strategy, Growth scenarios, Understanding Search                                                       | 20%     |
| Team Development                           | CDF Reviews, Career Development, Team Engagement, GitLab Values Coaching                                     | 15%     |
| Geo Product Management Support             | [Minimal support](https://about.gitlab.com/handbook/product/product-processes/#life-support-pm-expectations) | 15%     |
| Memory Product Management Support (acting) | [Minimal support](https://about.gitlab.com/handbook/product/product-processes/#life-support-pm-expectations) | 15%     |
| Database Product Management                | [Minimal support](https://about.gitlab.com/handbook/product/product-processes/#life-support-pm-expectations) | 5%      |
| Product Performance Indicators             | Performance Indicator instrumentation, understanding, goal setting and attainment                            | 0% (No capacity)      |
| Personal Growth / Leadership Opportunities | Representing GitLab externally, Representing Product internally                                              | 0% (No capacity)     |
| GTM Engagement                             | Use Case alignment, Opportunity Review, Sales Support, PMM/TMM Alignment                                     | 0% (No capacity)     |
| External Evangelism                        | Ops vision, analyst briefings, conference speaking                                                           | 0% (No capacity)     |
| Cross Section Product Experience           | Think Big-Think Small, Walk-throughs, learning goals, direction content review                               | 0% (No capacity)     |
| Core Team Engagement                       | UX, Development, Quality, Infrastructure shared Performance Indicators and Retrospectives                    | 0% (No capacity)     |
| Hiring                                     | Sourcing, interviewing, hiring (Frozen, subject to change)                                                   | 0% (Subject to change)     |

## Weekly Priorities

### Week 22 - 2021-05-31

**Sharding**

- [x] Get to a decision with regards to sharding strategy
- [x] Sharding operational support

**Memory**
- [x] Workout next steps for Memory endpoint work

**Geo**
- [x] Complete first iteration of [Backup and Restore business case](https://docs.google.com/document/d/1mCXNJS1bwtcWOXjphes9Pz1rRja3wSYVcCHfMHULTG0/edit?ts=609938e9)

**Product**
- [ ] [Finish additive bias MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/79553)
- [x] Review [L&D content](https://about.gitlab.com/handbook/product/product-manager-role/learning-and-development/#prioritization-at-gitlab) for Farnoosh 
- [x] Finish first interview for Memory/DB PM

### Week 21 - 2021-05-24

- Did not provide

### Week 20 - 2021-05-17

**Sharding**
- [x] Review sharding build board ahead of Monday meeting and coordinate with `@craig-gomes`
- [x] Review sharding build board ahead of Wednesday meeting and coordinate with `@craig-gomes`
- [x] Create an evaluation document of horiziontal application-level sharding
- [x] Investigate GitLab Region (started)
- [x] Sharding operational support

**Memory**
- [x] Kick off hiring for Memory/DB PM
- [ ] Workout next steps for Memory endpoint work

**Search**
- [ ] Review Global Search Business Case if available

**Geo***
- [x] [Finish handover](https://gitlab.com/gitlab-com/Product/-/issues/2535) to `nhxnguyen` to Geo acting PM
- [x] Review [Geo staffing plan](https://docs.google.com/document/d/1mCXNJS1bwtcWOXjphes9Pz1rRja3wSYVcCHfMHULTG0/edit?ts=609938e9)

**Product**
- [ ] [Finish additive bias MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/79553)

### Week 19 - 2021-05-10

**Short week. PTO on 2021-05-13 and 2021-05-14**

- [x] Yankees ROI
- [x] Stack rank epics with Memory
- [x] Review sharding build board ahead of Monday meeting and coordinate with `@craig-gomes`
- [x] Write release post items


### Week 18 - 2021-05-03

- [x] Sharding working group next steps
  - [x] Reach a decision on making GitLab's load balancer open source https://gitlab.com/gitlab-com/Product/-/issues/2459
  - [x] Review build board ahead of Monday meeting
  - [x] Review build board ahead of Wednesday meeting
  - [x] Support sharding working group operationally
- [x] Continue working on [Global Search direction](https://about.gitlab.com/direction/global-search/)
  - [x] Raise specific questions to John and CZ
  - [x] Review staffing request / bix case
- [x] Support team members
  - [x] Work on personal Readme to help team members understand how I work
- [x] Create hiring material / Review hiring plan
- [x] Personal development: Start reading [Effortless](https://gregmckeown.com/books/effortless/) (recommended by Nick Nguyen!)


### Week 17 - 2021-04-26

- [ ] Sharding working group next steps
  - [x] Get Glossary update merged https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/80393
  - [ ] Reach a decision on making GitLab's load balancer open source https://gitlab.com/gitlab-com/Product/-/issues/2459
  - [x] Support sharding working group operationally
- [x] Continue working on [Global Search direction](https://about.gitlab.com/direction/global-search/)
  - [x] Raise specific questions to John and CZ
- [x] Support team members
  - [x] Work on CDFs
  - [x] Summarize acitivites for Josh
- [ ] Create hiring material / Review hiring plan
- [x] Reduce meeting load this week by moving/cancelling meetings 

### Week 16 - 2021-04-19

- [x] Sharding working group next steps
  - [x] Review and update https://gitlab.com/groups/gitlab-org/-/epics/5759
  - [x] Read https://docs.google.com/document/d/1a6GWKYKgemMnkV1DlX5WwDpvCFJVgQ_zRgdGQOkcqZ4/edit?ts=6078c394
  - [x] Discuss OKRs
- [x] Familiarize myself with [Global Search direction](https://about.gitlab.com/direction/global-search/) and ongoing work
- [x] Review Geo's [Patroni work](https://gitlab.com/groups/gitlab-org/-/epics/4799) and next steps for GA
  - Also reviewed the Complete maturity epic and proposed reduced scope
- [x] Review ongoing Memory work in https://gitlab.com/groups/gitlab-org/-/epics/5622
- [x] Update [Enablement highlights](https://gitlab.com/gitlab-com/Product/-/issues/2412)

### Week 15 - 2021-04-12

- [x] Kickoff Sharding group work
- [x] Release preparation for Memory and Geo
