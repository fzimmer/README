# Fabian's README

Hello my name is Fabian Zimmer and I am the [Direcor of Product Mangement, SaaS Platforms](https://about.gitlab.com/job-families/product/product-management-leadership/#director-of-product-management) for the [SaaS Platforms section](https://about.gitlab.com/direction/saas-platforms/) here at GitLab.

### [whoami](https://en.wikipedia.org/wiki/Whoami)

- My family and close friends call me Fabi or Fabs. Everyone else calls me Fabian ([pronunciation](https://en.wiktionary.org/wiki/Fabian#German)). I prefer to be addressed by my first name rather than my last name + academic title, which is still common in German-speaking countries.
- My first language is standard or high German. I also understand some [Kölsch](https://en.wikipedia.org/wiki/Colognian) but I don't speak it.
- I live in [Münster](https://goo.gl/maps/vjSySpm3PSyGdryb9), Germany with my partner Tammela and daughter Emma. Emma is almost three years old and just learned how to ride a bike!
- My background is in evolutionary Biology and Bioinformatics. I received my Bachelor of Science and Master of Science from the University of Münster, and my PhD from University College London. If you are _really_ interested in my academic work, check out [my publications](https://qfma.de/publications/). I am always happy to talk about sex chromosome evolution, the evolution of [cephalopods](https://en.wikipedia.org/wiki/Cephalopod) (cuttlefish and squid) and any form of genome evolution!
- I like to cycle, cook, read, learn, and garden.
- I am a relatively private person and don't use most social media platforms. The fact that this readme is public should put this statement in perspective.

### General

- In my professional life, I've only ever worked for young companies or start ups. GitLab is the third and largest one.
- Peoples' interests may vary but nothing is boring. I love to hear about other folks' hobbies and passions and learn _why_ they enjoy that specific pursuit.
- I've spent a good amount of time in the UK and sometimes I am not as direct as I used to be.
- I believe [the Golden Rule](https://en.wikipedia.org/wiki/Golden_Rule) is often a good approximation for ethical behavior.
- My least favorite trait in people is arrogance. It implies a feeling of superiority and that can justify unethical behavior.

### Leadership

- I believe it is important to model behavior that is consistent with GitLab's values to inspire others to do the same.
- I believe we should celebrate highlights and share lowlights.
- I believe in the power of cross-functional teams. Collaboration is key to building great things.
- I believe curiosity and empathy are critical leadership traits.
- Diversity in all its dimensions is critical to solve complex problems. 

### Product

- I admire [Dieter Rams](https://en.wikipedia.org/wiki/Dieter_Rams) and very much agree with his 10 theses on "good design".
- Product Managers are not Project managers. Projects have a start and a defined end. Products don't. That being said, great Product Managers are able to manage cross-functional initiatives sucessfully.
- Processes should serve the team. There is more than one way to organize product development. Keep it simple and commit to using it consistently.

### How I work

- My timezone is [CET](https://time.is/CET)
- I wake up between 06:00 and 07:00 to go for a ride, care for my daughter, or do some chores around the house. I am pretty useless before a cup of coffee.
- I start my workday between 9:00 and 10:00.
- I enjoy coffee chats. My virtual door is always open. Whoever you are, please feel free to schedule one!
- I schedule most of my meetings in the afternoon to accommodate time zone differences but my productivity decreases drastically after 17:00. If you need me at maximum capacity, book a meeting before that.
- My daughter goes to bed around 19:00-19:30 and I prioritize helping her.
- I consider myself a fairly structured person. I need to sort things to make sense of them. Sometimes this adds overhead. Just let me know.
- If you need my attention, `@fzimmer` in issues will make sure I get to it. If you have an urgent request Slack is often faster
- I don't check my emails or Slack after work hours or on weekends.
- I disconnect completely for PTO but will organize points of contact in my absence.

### Personality test results

- My 16personalities results are [Protagonist, ENFJ-T](https://www.16personalities.com/enfj-personality).
